const express = require('express');
const mongoose = require('mongoose');
const app = express();

const port = 4000;

mongoose.connect("mongodb+srv://jing420:Wealthy2021!@b145.xyugj.mongodb.net/session32?retryWrites=true&w=majority",
		{
				useNewUrlParser : true,
				useUnifiedTopology : true 
		}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console,"There is an error with the connection"))
db.once("open",() => console.log("Successfully connected to the database"));

// //Middlewares ==> servers which provide common services and capabilities to our server
app.use(express.json())
app.use(express.urlencoded({extended: true}));




//Business Logic

// Mongoose userSchema

const userSchema = new mongoose.Schema({
	email: String,
	username: String,
	password: String,
	age: Number,
	isAdmin: {
		type: Boolean,
		default: false
	}
});

//Model - USER
const User = mongoose.model("User", userSchema);

//POST ROUTE TO REGISTER USER
app.post("/users/signup",(req,res) => {
	
	User.findOne({email : req.body.email}, (err,result)=> {

			if (result != null && result.email === req.body.email){

				return res.send(`Error, user already exists`)

			} else {

					let newUser = new User({

						email: req.body.email,
						username: req.body.username,
						password: req.body.password,
						age: req.body.age
					});
					
					newUser.save((saveErr, savedUser) => {

							if (saveErr){

									return console.error(saveErr)

							} else {

									return res.status(200).send(`New User added`)

							};
					});
			};	
		});
});


//Retrieving Users

app.get("/users", (req, res) => {

	User.find({}, (err, result) => {

		if(err){
			
			return console.log(err);
		
		} else {

			return res.status(200).json({
				user : result
			})
		}
	})
})

//UPDATING USER

app.put("/users/update-user/:wildcard", (req,res) => {
	let wildcard = req.params.wildcard
	let username = req.body.username

	User.findByIdAndUpdate(wildcard, {username : username}, (err, newUsername)=> {
		
		if(err){
			console.log(err)
		} else {

			res.send(`Username is now updated`);
		
		}
	
	})

});

//DELETE USER

app.delete("/users/archive-user/:wildcard",(req,res) => {
	
	let wildcard = req.params.wildcard;
	let deletedUser = req.body.username
	
	
	User.findByIdAndDelete(wildcard, (err,deletedUser) => {
	
		if(err){
	
			console.log(err)
	
		} else {
	
			res.send(`${deletedUser} has been deleted`)
		}
	})
})




//listenport
app.listen(port, () => console.log(`Server running at port ${port}`));